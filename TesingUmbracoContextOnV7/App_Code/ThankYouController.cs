﻿using System.Web.Mvc;
using Umbraco.Web.Mvc;

namespace TesingUmbracoContextOnV7.App_Code
{
    public class ThankYouController : RenderMvcController
    {
        public static string TransactionIdKey = "TransactionIdKey";
        public ActionResult Index(string paymentId)
        {
            var transactionId = Session[TransactionIdKey].ToString();
            var memberId = Members.GetCurrentMemberId();
            //if this was a real project, you will need to verify the paymentId and transactionId with the paymentgateway here.
            return Content("Session.IsNewSession = " + Session.IsNewSession + " transactionId = " + transactionId + " paymentId = " + paymentId + " memberId = " + memberId);
        }
    }
}