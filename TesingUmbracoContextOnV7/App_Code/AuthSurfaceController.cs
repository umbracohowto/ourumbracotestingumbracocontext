﻿using System.Web.Mvc;
using Umbraco.Web.Mvc;

namespace TesingUmbracoContextOnV7.App_Code
{
    public class AuthSurfaceController : SurfaceController
    {
        // GET: AuthSurface : In real life please dont make this a GET, have some respect for passwords :P 
        public ActionResult Login(string username, string password)
        {
            if (Members.Login(username, password))
            {
                return Redirect(Request.UrlReferrer.AbsolutePath);
            }
            return Content("Invalid credentials!!");
        }
    }
}